#pragma once

#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
#include "Cimg.h"


class Polygon : virtual public Shape
{
public:
	//c'tors and d'tors
	Polygon();
	Polygon(const string& type, const string& name);

	virtual ~Polygon();
protected:
	vector<Point> _points;
};