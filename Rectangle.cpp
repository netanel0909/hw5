#include "Rectangle.h"
myShapes::Rectangle::Rectangle(){}
myShapes::Rectangle::~Rectangle(){}

myShapes::Rectangle::Rectangle(const Point& other_p, double l, double w, const string& type, const string& name) :Shape(name, type)
{
	this->_p2 = Point(other_p.getX() + w, other_p.getY()+l);
	this->_p1 = other_p;
	_points.insert(_points.begin(), _p1);
	_points.insert(_points.begin(), this->_p2);
	this->_width = w;
	this->_length = l;

}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) 
{
	if (this->_color == NULL)
	{
		this->_color = manage_c();
	}
	board.draw_rectangle(_points[0].getY(), _points[0].getY(),_points[1].getX(), _points[1].getY(), this->_color, (float)300).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	if (this->_color == NULL)
	{
		this->_color = manage_c();
	}
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),_points[1].getX(), _points[1].getY(), this->_color, (float)300).display(disp);
	delete[] this->_color;
}

double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width;
}
double myShapes::Rectangle::getPerimeter() const
{
	return 2 * this->_width + 2 * this->_length;
}
void myShapes::Rectangle::move(const Point& other)
{
	this->_p1 += other;
	this->_p2 += other;
}
