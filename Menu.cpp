#include "Menu.h"

void Menu::Erase_all(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	char screen[] = { 0, 0, 0 };
	board.draw_rectangle(0, 0, 700, 700, screen,(float)300).display(disp);
	this->_shapes.clear();
}
void Menu::m_addShape()
{
	int op;
	cout << "Enter 0 to add a circle."<<endl<<
		"Enter 1 to add an arrow."<<endl<<
		"Enter 2 to add a triangle."<<endl<<
		"Enter 3 to add a rectangle." << endl;
	cin >> op;

	switch (op)
	{
	case 0:
		this->D_circle();
		break;
	case 1:
		this->D_arrow();
		break;
	case 2:
		this->D_tri();
		break;
	case 3:
		this->D_rect();
		break;
	default:
		system("cls");
		m_addShape();
		break;
	}
}
void Menu::m_modify()
{
	bool flag = true;
	while (flag)
	{
		int num;
		if (this->_shapes.empty() == true)
		{
			this->p_menu();
		}
		for (int i = 0; i < _shapes.size(); i++)
		{
			cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")" << "\n";
		}
		cin >> num;
		system("cls");
		flag = false;
		if (num < 0 || num > _shapes.size())
		{
			flag = true;
		}
		else
		{
			this->m_p_ls(num);
		}
	}
}
void Menu::m_p_ls(int num)
{

	int op;
	int x;
	int y;

	Point p;

	cout << "Enter 0 to move the shape" << endl<<
		"Enter 1 to get its details." << endl <<
		"Enter 2 to remove the shape." << endl;

	cin >> op;

	switch (op)
	{
	case 0:
		cout << "Please enter the X moving scale: ";
		cin >> x;
		cout << endl << "Please enter the Y moving scale: ";
		cin >> y;
		p = Point(x, y);
		this->_shapes[num]->clearDraw(*_disp, *_board);
		this->_shapes[num]->move(p);
		this->_shapes[num]->draw(*_disp, *_board);
		break;
	case 1:
		cout << this->_shapes[num]->getType() << "  " << this->_shapes[num]->getName() << "      " << this->_shapes[num]->getArea() << " " << this->_shapes[num]->getPerimeter() << "\n";
		system("pause");
		break;
	case 2:
		this->_shapes[num]->clearDraw(*_disp, *_board);
		this->_shapes.erase(_shapes.begin() + num);
		for (int i = 0; i < this->_shapes.size(); i++) {
			this->_shapes[i]->draw(*_disp, *_board);
		}
		break;
	default:
		system("cls");
		m_p_ls(num);
		break;
	}
}


void Menu::D_arrow()
{
	Arrow* a1 = new Arrow();
	Point p1;
	Point p2;
	int x;
	int y;
	string shape_n;
	cout << "Enter the X of point number: 1" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y;
	p1 = Point(x, y);
	cout << "Enter the X of point number: 2" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y;
	cout << "Please enter the name of the shape:" << endl;
	cin >> shape_n;
	p2 = Point(x, y);
	a1 = new Arrow(p1, p2, "Arrow", shape_n);
	a1->draw(*this->_disp, *this->_board);
	this->_shapes.insert(_shapes.end(), a1);
}
void Menu::D_rect()
{
	myShapes::Rectangle* r1;
	Point p1;

	double x;
	double y;
	double l;
	double w;

	string name;

	cout << "Enter the X of the to left corner:";
	cin >> x;
	cout << "Enter the Y of the top left corner:";
	cin >> y;
	cout << "Please enter the length of the shape:";
	cin >> l;
	cout << "Please enter the width of the shape:";
	cin >> w;
	cout << "Enter the name of the shape:";
	cin >> name;
	p1 = Point(x, y);
	if (!(l&&w))
	{
		cout << "Length or width cant be zero" << endl;
		system("pause");
		system("cls");
		p_menu();
	}
	r1 = new myShapes::Rectangle(p1, l, w,"Rectangle",name);
	r1->draw(*_disp,*_board);
	this->_shapes.insert(_shapes.end(), r1);

}

void Menu::D_tri()
{

	int x;
	int y;

	string name;
	cout << "Enter the X of point number: 1" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y;
	Point p1 = Point(x, y);
	cout << "Enter the X of point number: 2" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y;
	Point p2 = Point(x, y);
	cout << "Enter the X of point number: 3" << endl;
	cin >> x;
	cout << "Enter the Y of point number: 3" << endl;
	cin >> y;
	Point p3 = Point(x, y);

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;
	if ((p1.getY() - p3.getY())/ (p1.getX() - p3.getX()) == (p1.getY() - p2.getY()) / (p1.getX() - p2.getX()))
	{
		cout << "The points entered created a line" << endl;
		system("pause");
		system("cls");
		p_menu();
	}
	Triangle* t1 = new Triangle(p1, p2, p3, "triangle", name);
	t1->draw(*_disp, *_board);
	this->_shapes.insert(_shapes.end(), t1);
}

void Menu::D_circle()
{
	double x;
	double y;
	double rad;

	string name;
	cout << "Please enter X:" << endl;
	cin >> x;
	cout << "Please enter y:" << endl;
	cin >> y;
	cout << "Please enter radius:" << endl;
	cin >> rad;
	cout << "Please enter the name of the shape:" << endl;
	cin >> name;
	Point p1 = Point(x, y);
	Circle* c1 = new Circle(p1, rad, "Circle", name);
	c1->draw(*this->_disp, *this->_board);
	this->_shapes.insert(_shapes.end(), c1);
}

Menu::Menu() 
{
	_board  = new cimg_library::CImg <unsigned char> (700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::p_menu()
{
	int i = 0;
	int op = 0;
	while (op != 3)
	{
		cout << "Enter 0 to add a new shape." << endl
			<< "Enter 1 to modify or get information from a current shape." << endl
			<< "Enter 2 to delete all of the shapes." << endl
			<< "Enter 3 to exit." << endl;
		cin >> op;

		switch (op)
		{
		case 0:

			system("cls");
			this->m_addShape();
			break;
		case 1:

			system("cls");
			this->m_modify();
			break;
		case 2:
			system("cls");
			this->Erase_all(*this->_disp, *this->_board);
			break;
		}
		system("cls");
	}
	exit(0);
}