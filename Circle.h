#pragma once
#include<iostream>
#include <math.h>
#include "Shape.h"
#include "Point.h"
#include <vector>
#define PI 3.14



class Circle : public Shape
{
public:
	//c'tors and d'tors
	Circle();
	Circle(const Point& center, double radius, const string& type, const string& name);
	~Circle();

	const Point& getCenter() const;//retruns the shapes center point
	double getRadius() const;//returns the shapes radious

	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);//draws shape
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);//deletes the draw from the board
	virtual double getArea() const;//returns the shapes area
	virtual double getPerimeter() const;//returns the shapes perimeter
	virtual void move(const Point& other);//moves the shape
private:
	Point pos;
	double _rad;
};