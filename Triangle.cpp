#include "Triangle.h"
Triangle::Triangle(){}
Triangle::~Triangle(){}

Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3, const string& type, const string& name) :Shape(name, type)
{
	this->_p1 = p1;
	_points.insert(_points.begin(), _p1);
	this->_p2 = p2;
	_points.insert(_points.begin(), _p2);
	this->_p3 = p3;
	_points.insert(_points.begin(), _p3);
}
double Triangle::getArea() const
{
	return haron_formula(this->_p1, this->_p2,this->_p3);
}
double Triangle::getPerimeter() const
{
	return this->_p1.distance(this->_p2) + this->_p2.distance(this->_p3) + this->_p3.distance(this->_p1);
}
void Triangle::move(const Point& other) 
{
	this->_p1 += other;
	this->_p2 += other;
	this->_p3 += other;

	return;
}
double Triangle::haron_formula(Point p1, Point p2, Point p3) const
{
	double a = p1.distance(p2);
	double b = p2.distance(p3);
	double c = p3.distance(p1);
	double s = (a + b + c) / 2;

	return sqrt(s*(s - a)*(s - b)*(s - c));
}
void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	if (this->_color == NULL)
	{
		this->_color = manage_c();
	}
	board.draw_triangle(_points[0].getX(), _points[0].getY(),_points[1].getX(), _points[1].getY(),_points[2].getX(), _points[2].getY(), this->_color, (float)300).display(disp);
	delete[] this->_color;
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	char color[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),_points[1].getX(), _points[1].getY(),_points[2].getX(), _points[2].getY(), color, (float)300).display(disp);
}
