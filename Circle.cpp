#include "Circle.h"
Circle::Circle(){}
Circle::~Circle(){}

Circle::Circle(const Point& center, double radius, const string& type, const string& name) :Shape(name, type)
{
	this->pos = center;
	this->_rad = radius;
}
const Point& Circle::getCenter() const
{
	return this->pos;
}
double Circle::getRadius() const
{
	return this->_rad;
}

double Circle::getArea() const
{
	return (PI * pow(_rad , 2));
}
double Circle::getPerimeter() const
{
	return (PI * 2 * _rad);
}
void Circle::move(const Point& other)
{
	this->pos = other;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	if (this->_color == NULL)
	{
		this->_color = manage_c();
	}
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), this->_color, (float)300).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char color[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), color, (float)300).display(disp);
}


