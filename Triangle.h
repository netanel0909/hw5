#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
public:
	//c'tors and d'tors
	Triangle();
	Triangle(const Point& p1, const Point& p2, const Point& p3, const string& type, const string& name);
	virtual ~Triangle();
	//draw and erase shape
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	virtual double getArea() const;//returns area and peri
	virtual double getPerimeter() const;
	virtual void move(const Point& other);//moves tri over the board
private:
	double haron_formula(Point p1, Point p2, Point p3) const;//calcs tri area with herons formula
	Point _p1;
	Point _p2;
	Point _p3;
};