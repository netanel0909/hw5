#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	//c'tors and d'tors
	Arrow();
	Arrow(const Point& p1, const Point& p2, const string& type, const string& name);
	~Arrow();

	virtual double getPerimeter() const;//returns the arrows perimeter
	virtual void move(const Point& other);//movs the arrow on the screen
	virtual double getArea() const;	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);//puts the arrows area

	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);//draws arrow

private:
	Point _p1;
	Point _p2;
};