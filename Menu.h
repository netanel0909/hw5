#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <map>
#include <iostream>
#include <sstream>
#include <vector>
class Menu
{
public:

	Menu();
	~Menu();
	void p_menu();//main menu
	void m_addShape();//menu of adding a new shape
	void m_modify();//menu of shape modefing
	void m_p_ls(int a);//list of shapes
	void Erase_all(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);//clears the board

	void D_circle();//circle menu
	void D_arrow();//arrow menu
	void D_rect();//rect menu
	void D_tri();//tri menu


private:
	vector<Shape*> _shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

