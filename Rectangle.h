#pragma once
#include "Polygon.h"
#include <vector>

namespace myShapes
{
	class Rectangle :virtual public Polygon
	{
	public:
		//c'tors and d'tors
		Rectangle();
		Rectangle(const Point& other_p, double l, double w, const string& type, const string& name);
		virtual ~Rectangle();
		//draw function of rect
		void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)  override;
		//deletes rect
	    void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)  override;

		virtual double getArea() const;//returns irs area and peri
		virtual double getPerimeter() const;
		virtual void move(const Point& other); //moves the rectangle
	private:
		Point _p1;
		Point _p2;

		double _length;
		double _width;
	};
}