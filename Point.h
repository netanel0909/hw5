#pragma once
#include <vector>
#include<iostream>
#include "CImg.h"


class Point
{
public:
	//c'tors and d'tors
	Point();
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();
	
	Point operator+(const Point& other) const;//points addition
	Point& operator+=(const Point& other);

	double getX() const;//return x and y of point
	double getY() const;
	double distance(const Point& other) const;//calcs dis bitween 2 points

private:
	double _x;
	double _y;
};