#pragma once
#include <iostream>
#include "Point.h"
#include "CImg.h"
#include <map> 
#include<string>
#include <vector>

using namespace std;

class Shape 
{
public:
	//c'tors and d'tors
	Shape();
	Shape(const string& name, const string& type);
	//color managment
	char* manage_c(void);
	//returns area and peri
	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	//draws shape
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0;
	virtual void move(const Point& other) = 0;//moves shape over the board
	void printDetails() const;//prints shaps details
	string getType() const;//returns shapes type and name
	string getName() const;
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board) = 0;//clears the drawing
	char* _color;

private:
	string _name;
	string _type;

protected:
	vector<cimg_library::CImg<unsigned char>> _graphicShape;
};