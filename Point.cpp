#include "Point.h"
#include<math.h>
Point::Point(){}
Point::~Point(){}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

Point Point::operator+(const Point & other) const
{
	return Point(this->_x+other._x , this->_y+other._y);
}

Point & Point::operator+=(const Point & other)
{

	this->_x += other.getX();
	this->_y += other.getY();

	return *this;
}

double Point::distance(const Point& other) const
{
	int m = pow(this->getX() - other.getX(), 2);
	int n = pow(this->getY() - other.getY(), 2);
	return sqrt(m + n);
}