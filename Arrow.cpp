#include "Arrow.h"
Arrow::Arrow(){}
Arrow::~Arrow(){}

Arrow::Arrow(const Point& p1, const Point& p2, const string& type, const string& name) :Shape(name, type)
{
	this->_p1 = p1;
	this->_p2 = p2;
}

void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	if (this->_color == NULL)
	{
		this->_color = manage_c();
	}
	board.draw_arrow(_p1.getX(), _p1.getY(), _p2.getX(), _p2.getY(), this->_color, (float)300).display(disp);
	delete[] this->_color;
}

void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	char color[] = { 0, 0, 0 };
	board.draw_arrow(_p1.getX(), _p1.getY(),_p2.getX(), _p2.getY(), color, (float)300).display(disp);
}

double Arrow::getArea() const
{
	return 0;
}
double Arrow::getPerimeter() const
{
	return this->_p1.distance(this->_p2);
}
void Arrow::move(const Point& other)
{
	this->_p1 += other;
	this->_p2 += other;
}

